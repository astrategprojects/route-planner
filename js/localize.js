'use strict'
angular.module('localization', []).
	factory('localize', ['$http', '$rootScope', '$window', '$filter', function($http, $rootScope, $window, $filter) {
		var localize = {
			language: $window.navigator.userLanguage || $window.navigator.language,
			dictionary: [],
			resourceFileLoaded: false,
			successCallback: function(data) {
				localize.dictionary = data;
				localize.resourceFileLoaded = true;
				$rootScope.$broadcast('localizeResourcesUpdates');
			},
			initLocalizedResources: function() {
				var url = 'http://www.xn--9dbccjlkfq.com/route-planner-widget/js/i18n/resources-locale_' + localize.language + '.js';
				$http({
					method: 'GET',
					url: url,
					cache: false
				}).success(localize.successCallback).error(function() {
					var url = 'http://www.xn--9dbccjlkfq.com/route-planner-widget/js/i18n/resources-locale_default.js';
					$http({
						method: 'GET',
						url: url,
						cache: false
					}).success(localize.successCallback);
				});
			},
			getLocalizeString: function(key) {
	            var result = '';
	            if (!localize.resourceFileLoaded) {
	                localize.initLocalizedResources();
	                localize.resourceFileLoaded = true;
	                return result;
	            }
	            if ((localize.dictionary !== []) && (localize.dictionary.length > 0)) {
	                var entry = $filter('filter')(localize.dictionary, key)[0];
	                if ((entry !== null) && (entry != undefined)) {
		                    result = entry.value;
		            }
		        }
		        return result;
			}
		};

		return localize;
}]).
	filter('i18n', ['localize', function(localize) {
	return function(input) {
		return localize.getLocalizeString(input);
	}
}]);
