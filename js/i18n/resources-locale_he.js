[
	{
		"key":"_Title_",
		"value":"תכנון מסלול",
		"description":"The title of the widget"
	},
	{
		"key":"_OriginLabel_",
		"value":"מקור",
		"description":"The hebrew translation of the word Origin"
	},
	{
		"key":"_DestinationLabel_",
		"value":"יעד",
		"description":"The hebrew translation of the word Destination"
	},
	{
		"key":"_DepartureLabel_",
		"value":"יציאה",
		"description":"The hebrew translation of the word Departure"
	},
	{
		"key":"_SearchLabel_",
		"value":"לחפש",
		"description":"The hebrew translation of the word Search"
	},
	{
		"key":"_SettingsLabel_",
		"value":"הגדרות",
		"description":"The hebrew translation of the word Settings"
	},
	{
		"key":"_DefaultLanguageLabel_",
		"value":"שפת ברירת מחדל",
		"description":"The hebrew translation of the word Default Language"
	},
	{
		"key":"_DistanceMessage_",
		"value":"מרחק בין המקור והיעד הוא",
		"description":"The distance message string when prompting the calculated distance"
	},
	{
		"key":"_DurationMessage_",
		"vale":"מסע בזמן הוא",
		"description":"The travel time message string when prompting the calculated travel time"
	},
	{
		"key":"_DrivingLabel_",
		"value":"נהיגה",
		"description":"The hebrew translation of the word Driving"
	},
	{
		"key":"_TransitLabel_",
		"value":"תחבורה ציבורית",
		"description":"The hebrew translation of the word Transit"
	},
	{
		"key":"_TravelModeLabel_",
		"value":"כיצד תרצה להגיע ליעד",
		"description":"The travel mode label"
	},
	{
		"key":"_TransportationLabel_",
		"value":"בחר רכב התחבורה שלך",
		"description":"The transportation label"
	},
	{
		"key":"_RoutesResultLabel_",
		"value":"מסלולי תוצאות",
		"description":"The Routes result label"	
	},
	{
		"key":"_RouteSuggestionLabel_",
		"value":"הצעה מסלולים",
		"description":"The routes suggestion label"	
	},
	{
		"key":"_SearchNewLabel_",
		"value":"חיפוש חדש",
		"description":"The search new label"	
	},
	{
		"key":"_RecentSearchesLabel_",
		"value":"חיפושים אחרונים",
		"description":"The recent searches label"
	},
	{
		"key":"_ViaLabel_",
		"value":"באמצעות",
		"description":"The Via label"
	},
	{
		"key":"_SendingRequestLabel_",
		"value":"מחשב את המסלול, אנא המתן...",
		"description":"The sending request message"
	},
	{
		"key":"_DrivingLabel_",
		"value":"נהיגה",
		"description":"The driving label"
	},
	{
		"key":"_TransitLabel_",
		"value":"תחבורה ציבורית",
		"description":"The transit label"
	},
	{
		"key":"_DepartureTimeLabel_",
		"value":"זמן יציאה",
		"description":"The departure time label"
	},
	{
		"key":"_TravelTimeLabel_",
		"value":"מסע בזמן",
		"description":"The travel time label"
	},
	{
		"key":"_ClearLabel_",
		"value":"נקה",
		"description":"The clear route label"
	},
	{
		"key":"_LanguageLabel_",
		"value":"שפה",
		"description":"The Language label"
	},
	{
		"key":"_DefaultPlace_",
		"value":"מקום ברירת מחדל",
		"description":"The default place label"
	},
	{
		"key":"_Bus_",
		"value":"אוטובוס",
		"description":"The bus label"
	},
	{
		"key":"_Train_",
		"value":"רכבת",
		"description":"The train label"
	},
	{
		"key":"_Tram_",
		"value":"רכבת קלה",
		"description":"The tram label"
	},
	{
		"key":"_Subway_",
		"value":"רכבת תחתית",
		"description":"The tram label"
	},
	{
		"key":"_Messages1_",
		"value":"המתן בבקשה, המערכת מחשבת עבורך את המסלול הטוב ביותר...",
		"description":"The messages shown in request dialog box"
	},
	{
		"key":"_Messages2_",
		"value":"שמירת ההגדרות שלך ב 3 שניות...",
		"description":"The messages shown in request dialog box"
	},
	{
		"key":"_SaveSettings_",
		"value":"לשמור את הגדרות",
		"description":"the save settings button"
	}
]