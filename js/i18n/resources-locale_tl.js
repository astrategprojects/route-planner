[
	{
		"key":"_Title_",
		"value":"Route Planner",
		"description":"The title of the widget"
	},
	{
		"key":"_OriginLabel_",
		"value":"Pinagmulan",
		"description":"The english translation of the word Origin"
	},
	{
		"key":"_DestinationLabel_",
		"value":"Patutunguhan",
		"description":"The english translation of the word Destination"
	},
	{
		"key":"_DepartureLabel_",
		"value":"Oras ng Pag-alis",
		"description":"The english translation of the word Departure"
	},
	{
		"key":"_SearchLabel_",
		"value":"Hanapin",
		"description":"The english translation of the word Search"
	},
	{
		"key":"_SettingsLabel_",
		"value":"Mga Setting",
		"description":"The english translation of the word Settings"
	},
	{
		"key":"_DefaultLanguageLabel_",
		"value":"Wika",
		"description":"The english translation of the word Default Language"
	},
	{
		"key":"_DistanceMessage_",
		"value":"Ang distansya sa pagitan ng Pinagmulan at Patutunahan ay",
		"description":"The distance message string when prompting the calculated distance"
	},
	{
		"key":"_DurationMessage_",
		"vale":"Kabuuang oras ng biyahe",
		"description":"The travel time message string when prompting the calculated travel time"
	},
	{
		"key":"_DrivingLabel_",
		"value":"Maneho",
		"description":"The english translation of the word Driving"
	},
	{
		"key":"_TransitLabel_",
		"value":"Transportasyon",
		"description":"The english translation of the word Transit"
	},
	{
		"key":"_TravelModeLabel_",
		"value":"Pumili ng uri ng transportasyon",
		"description":"The travel mode label"
	},
	{
		"key":"_TransportationLabel_",
		"value":"Pumili ng uri ng transportasyong sasakyan",
		"description":"The transportation label"
	},
	{
		"key":"_RoutesResultLabel_",
		"value":"Ang mga posibleng ruta",
		"description":"The Routes result label"	
	},
	{
		"key":"_RouteSuggestionLabel_",
		"value":"Posibleng ruta",
		"description":"The routes suggestion label"	
	},
	{
		"key":"_SearchNewLabel_",
		"value":"Maghanap ulit",
		"description":"The search new label"	
	},
	{
		"key":"_RecentSearchesLabel_",
		"value":"Mga hinanap kanina",
		"description":"The recent searches label"
	},
	{
		"key":"_ViaLabel_",
		"value":"Via",
		"description":"The Via label"
	},
	{
		"key":"_SendingRequestLabel_",
		"value":"Nag papadala ng request ...",
		"description":"The sending request message"
	},
	{
		"key":"_DrivingLabel_",
		"value":"Maneho",
		"description":"The driving label"
	},
	{
		"key":"_TransitLabel_",
		"value":"Transportasyon",
		"description":"The transit label"
	},
	{
		"key":"_DepartureTimeLabel_",
		"value":"Oras ng pag-alis",
		"description":"The departure time label"
	},
	{
		"key":"_TravelTimeLabel_",
		"value":"Oras ng biyahe",
		"description":"The travel time label"
	},
	{
		"key":"_ClearLabel_",
		"value":"Burahin",
		"description":"The clear route label"
	},
	{
		"key":"_LanguageLabel_",
		"value":"Wika",
		"description":"The Language label"
	},
	{
		"key":"_DefaultPlace_",
		"value":"Default na Lugar",
		"description":"The default place label"
	},
	{
		"key":"_Bus_",
		"value":"Bus",
		"description":"The bus label"
	},
	{
		"key":"_Train_",
		"value":"Tren",
		"description":"The train label"
	},
	{
		"key":"_Tram_",
		"value":"Tram",
		"description":"The tram label"
	},
	{
		"key":"_Subway_",
		"value":"Subway",
		"description":"The tram label"
	},
	{
		"key":"_Messages1_",
		"value":"Nag se-send ng request, ilang sandali lamang",
		"description":"The sending request message"
	},
	{
		"key":"_Messages2_",
		"value":"Sinisave ang settings, 3 segundo lamang...",
		"description":"The messages shown in request dialog box"
	},
	{
		"key":"_SaveSettings_",
		"value":"i-Save ang Settings",
		"description":"the save settings button"
	}
]
