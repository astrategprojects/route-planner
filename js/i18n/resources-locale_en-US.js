[
	{
		"key":"_Title_",
		"value":"Route Planner",
		"description":"The title of the widget"
	},
	{
		"key":"_OriginLabel_",
		"value":"Origin",
		"description":"The english translation of the word Origin"
	},
	{
		"key":"_DestinationLabel_",
		"value":"Destination",
		"description":"The english translation of the word Destination"
	},
	{
		"key":"_DepartureLabel_",
		"value":"Departure",
		"description":"The english translation of the word Departure"
	},
	{
		"key":"_SearchLabel_",
		"value":"Search",
		"description":"The english translation of the word Search"
	},
	{
		"key":"_SettingsLabel_",
		"value":"Settings",
		"description":"The english translation of the word Settings"
	},
	{
		"key":"_DefaultLanguageLabel_",
		"value":"Default Language",
		"description":"The english translation of the word Default Language"
	},
	{
		"key":"_DistanceMessage_",
		"value":"Distance between Origin and Destination is",
		"description":"The distance message string when prompting the calculated distance"
	},
	{
		"key":"_DurationMessage_",
		"vale":"Travel Time is",
		"description":"The travel time message string when prompting the calculated travel time"
	},
	{
		"key":"_DrivingLabel_",
		"value":"Driving",
		"description":"The english translation of the word Driving"
	},
	{
		"key":"_TransitLabel_",
		"value":"Transit",
		"description":"The english translation of the word Transit"
	},
	{
		"key":"_TravelModeLabel_",
		"value":"Choose your travel mode",
		"description":"The travel mode label"
	},
	{
		"key":"_TransportationLabel_",
		"value":"Choose your transportation vehicle",
		"description":"The transportation label"
	},
	{
		"key":"_RoutesResultLabel_",
		"value":"Routes Result",
		"description":"The Routes result label"	
	},
	{
		"key":"_RouteSuggestionLabel_",
		"value":"Route Suggestion",
		"description":"The routes suggestion label"	
	},
	{
		"key":"_SearchNewLabel_",
		"value":"Search New",
		"description":"The search new label"	
	},
	{
		"key":"_RecentSearchesLabel_",
		"value":"Recent searches",
		"description":"The recent searches label"
	},
	{
		"key":"_ViaLabel_",
		"value":"Via",
		"description":"The Via label"
	},
	{
		"key":"_SendingRequestLabel_",
		"value":"Sending a request, please wait ...",
		"description":"The sending request message"
	},
	{
		"key":"_DrivingLabel_",
		"value":"Driving",
		"description":"The driving label"
	},
	{
		"key":"_TransitLabel_",
		"value":"Transit",
		"description":"The transit label"
	},
	{
		"key":"_DepartureTimeLabel_",
		"value":"Departure Time",
		"description":"The departure time label"
	},
	{
		"key":"_TravelTimeLabel_",
		"value":"Travel Time",
		"description":"The travel time label"
	},
	{
		"key":"_ClearLabel_",
		"value":"Clear",
		"description":"The clear route label"
	},
	{
		"key":"_LanguageLabel_",
		"value":"Language",
		"description":"The Language label"
	},
	{
		"key":"_DefaultPlace_",
		"value":"Default Place",
		"description":"The default place label"
	},
	{
		"key":"_Bus_",
		"value":"Bus",
		"description":"The bus label"
	},
	{
		"key":"_Train_",
		"value":"Train",
		"description":"The train label"
	},
	{
		"key":"_Tram_",
		"value":"Tram",
		"description":"The tram label"
	},
	{
		"key":"_Subway_",
		"value":"Subway",
		"description":"The tram label"
	},
	{
		"key":"_Messages1_",
		"value":"Sending a request, please wait...",
		"description":"The messages shown in request dialog box"
	},
	{
		"key":"_Messages2_",
		"value":"Saving your settings in 3 seconds...",
		"description":"The messages shown in request dialog box"
	},
	{
		"key":"_SaveSettings_",
		"value":"Save Settings",
		"description":"the save settings button"
	}
]
