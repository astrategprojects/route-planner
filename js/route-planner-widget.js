/* weather widget js */
angular.module('localizeApp', ['localization']).
config(['$routeProvider', function ($routeProvider) {
    $routeProvider.
        when('/', {templateUrl:'route-planner-widget/route-planner-widget.html', controller:RoutePlannerCtrl}).
        otherwise({redirectTo:'/'});
}]);

var RoutePlannerCtrl = function($scope) {

	$scope.ui = {
		map: '',
		origin: '',
		destination: '',
		departure: 'Departure',
		languages: [{value:'en', name:'English'}, {value:'he', name:'Hebrew'}, {value:'tl', name:'Tagalog'}],
		default_place: 'Neve Yarak, Israel',
		default_language: 'he',
		routes_shown: 0,
		previous_searches: [],
		travelmode: [{mode:"תחבורה ציבורית", inmap: "TRANSIT"}, {mode:"נסיעה ברכב", inmap: "DRIVING"}],
		setMapLanguage: function() {
			if ( typeof Storage != 'undefined' ) {
				if ( localStorage.getItem('ui.default_language') != null ) {
					localStorage.setItem('ui.default_language', this.default_language);
				}
			}
		},
		setDefaultPlace: function() {
			if ( typeof Storage != 'undefined' ) {
				if ( localStorage.getItem('ui.default_place') != null ) {
					localStorage.setItem('ui.default_place', this.default_place);
				}
			}
		},
		loadLanguage: function() {
			if ( typeof Storage != 'undefined' ) {
				if ( localStorage.getItem('ui.default_language') != null ) {
					this.default_language = localStorage.getItem('ui.default_language');
				}
				else {
					localStorage.setItem('ui.default_language', this.default_language);
				}
			}
		},
		loadDefaultPlace: function() {
			if ( typeof Storage != 'undefined' ) {
				if ( localStorage.getItem('ui.default_place') != null ) {
					this.default_place = localStorage.getItem('ui.default_place');
				}
				else {
					localStorage.setItem('ui.default_place', this.default_place);
				}
			}	
		}
	};

	$scope.mapapi = {
		url: 'http://maps.googleapis.com/maps/api/directions/json?',
		origin: 'origin=',
		destination: '&destination=',
		origin_data: '',
		destination_data: '',
		travelmode_data: '',
		sensor: '&sensor=false',
		mode: '&mode=transit',
		departure_time: '',
		departure_time_data: '',
		alternatives: '&alternatives=true',
		distance_calculated: '',
		duration_calculated: '',
		request: '',
		message: '',
		steps: '',
		legs: '',
		routes: '',
		travel_time: '',
		directionsRenderer: ''
	};

	$scope.vehicle_types = [
		{ 
			name: "Tram", 
			image: "http://maps.gstatic.com/mapfiles/transit/iw/6/tram.png", 
			alt: "רכבת",
			value: true
		},
		// { 
		// 	name: "Rail", 
		// 	image: "http://maps.gstatic.com/mapfiles/transit/iw/6/rail.png", 
		// 	value: true
		// },
		{ 
			name:"Bus", 
			image:"http://maps.gstatic.com/mapfiles/transit/iw/6/bus.png",
			alt:"אוטובוס",
			value: true
		}
		// { 
		// 	name: "Subway", 
		// 	image: "http://maps.gstatic.com/mapfiles/transit/iw/6/subway.png", 
		// 	value: true
		// }
	];

	function setSteps(routes, marking) {
		var counter = 0;

		var hours = Math.floor(counter / 60);
		var minutes = counter % 60;

		if ( typeof marking.mark != 'undefined' && marking.mark == 1 ) {
			for (var i = 0; i < routes.length; i++) {
				$scope.mapapi.steps.push( routes[i].legs[0].steps );
				$scope.mapapi.legs.push( routes[i].legs[0] );
				$scope.mapapi.routes.push( routes[i] ); 
			}
		}
		else {
			$scope.$apply(function() {
				var route = [];
				for (var i = 0; i < routes.length; i++) {
					var routeobj = {

					};

					$scope.mapapi.steps = routes[i].legs[0].steps;
					$scope.mapapi.legs = routes[i].legs[0];
					route.push( routes[i] ); 
				}

				$scope.mapapi.routes = route;
				console.log($scope.mapapi.routes);
			});
		}

		filterResult();
		bootbox.hideAll();
		console.log('message is cleared');
	}

	function clearMessage() {
		$scope.$apply(function() {
			$scope.mapapi.message = '';
		});
		console.log('message is cleared');
	}

	function blankFields() {
		angular.element('#origin').val('');
		angular.element('#destination').val('');
	}

	function addGoogleSearchListener(field) {
		google.maps.event.addListener(field, 'places_changed', function() {
  			var markers;
  			var places = field.getPlaces();

		    for (var i = 0, marker; marker = markers[i]; i++) {
		      marker.setMap(null);
		    }

		    // For each place, get the icon, place name, and location.
		    markers = [];
		    var bounds = new google.maps.LatLngBounds();
		    for (var i = 0, place; place = places[i]; i++) {
		      var image = {
		        url: place.icon,
		        size: new google.maps.Size(71, 71),
		        origin: new google.maps.Point(0, 0),
		        anchor: new google.maps.Point(17, 34),
		        scaledSize: new google.maps.Size(25, 25)
		      };

		      // Create a marker for each place.
		      var marker = new google.maps.Marker({
		        map: $scope.ui.map,
		        icon: image,
		        title: place.name,
		        position: place.geometry.location
		      });

		      markers.push(marker);

		      bounds.extend(place.geometry.location);
		    }

		    $scope.ui.map.fitBounds(bounds);
  		});

  		google.maps.event.addListener($scope.ui.map, 'bounds_changed', function() {
			var bounds = $scope.ui.map.getBounds();
			field.setBounds(bounds);
		});
	}

	function initializeMap(legs, initial) {
		if (typeof legs == 'undefined') {
			var mapOptions = {
				zoom:8,
				center: new google.maps.LatLng(-34.397, 150.644)
			};
			$scope.ui.map = new google.maps.Map(angular.element('#map-canvas')[0], mapOptions);
		}
		else {
			var directionsDisplay = new google.maps.DirectionsRenderer();
			$scope.ui.origin = new google.maps.LatLng(legs.start_location.lat, legs.start_location.lng);
			$scope.ui.destination = new google.maps.LatLng(legs.end_location.lat, legs.end_location.lng);
			var mapOptions = {
				zoom: 12,
				center: $scope.ui.origin
			};
			$scope.ui.map = new google.maps.Map(angular.element('#map-canvas')[0], mapOptions);
			if (typeof initial == "undefined") {
				directionsDisplay.setMap($scope.ui.map);
				calcRoute($scope.ui.origin, $scope.ui.destination, 0, true);
			}
		}

	  	addGoogleSearchListener( new google.maps.places.SearchBox((angular.element('#origin')[0])) );
  		addGoogleSearchListener( new google.maps.places.SearchBox((angular.element('#destination')[0])) );
	}

	function filterResult() {
		if ( jQuery('#travelmode').val() == 'TRANSIT' ) {
			jQuery('fieldset.steps').each(function() { 
				jQuery(this).hide(); 
			});
			jQuery('input[type="checkbox"]:checked').each(function() {
				var checkbox = jQuery(this);
				jQuery('fieldset.steps').each(function() {
	                var labelclass = checkbox.parent('label').attr('class');
					if ( jQuery(this).find('.' + labelclass).length > 0 ) {
						jQuery(this).show();
					}
				});
			});
		}
	}

	function getRendererOptions(main_route) {
		if(main_route) {
			var _colour = '#00458E';
			var _strokeWeight = 4;
			var _strokeOpacity = 1.0;
			var _suppressMarkers = false;
		}
		else {
			var _colour = '#ED1C24';
			var _strokeWeight = 2;
			var _strokeOpacity = 0.7;
			var _suppressMarkers = false;
		}
		var polylineOptions = { 
			strokeColor: _colour, 
			strokeWeight: _strokeWeight, 
			strokeOpacity: _strokeOpacity  
		};
		var rendererOptions = {
			draggable: false, 
			suppressMarkers: _suppressMarkers, 
			polylineOptions: polylineOptions
		};
		return rendererOptions;
	}

	function renderDirections(result, rendererOptions, routeToDisplay, clear) {
		if( routeToDisplay == 0 ) {
			var _colour = '#00458E';
			var _strokeWeight = 4;
			var _strokeOpacity = 1.0;
			var _suppressMarkers = false;
		}
		else {
			var _colour = '#ED1C24';
			var _strokeWeight = 4;
			var _strokeOpacity = 0.7;
			var _suppressMarkers = false;
		}

		// create new renderer object
		var directionsRenderer = new google.maps.DirectionsRenderer({
			draggable: false, 
			suppressMarkers: _suppressMarkers, 
			polylineOptions: { 
				strokeColor: _colour, 
				strokeWeight: _strokeWeight, 
				strokeOpacity: _strokeOpacity  
			},
			preserveViewport:true
		});
		
		directionsRenderer.setMap($scope.ui.map);
		directionsRenderer.setDirections(result);
		directionsRenderer.setRouteIndex(routeToDisplay);
	}

	function calcRoute(origin, destination, routeIndex, main_route) {
		var directionsService = new google.maps.DirectionsService();
		var request = {
			origin: origin,
			destination: destination,
			travelMode: google.maps.TravelMode[angular.element('#travelmode').val()],
			provideRouteAlternatives: true
		};
		directionsService.route(request, function(response, status) {
			if ( status == google.maps.DirectionsStatus.OK ) {
				if (main_route == true) {
					setSteps(response.routes, { mark:0 });
					var renderOptions = getRendererOptions(false);
					renderDirections(response, renderOptions, routeIndex, true);
				}
				else {
					var rendererOptions = getRendererOptions(true);
					renderDirections(response, rendererOptions, routeIndex);
				}
			}
		});
	}

	function makeRequest(proxyUrl, initial) {
		var stored = storedData(initial);
		if ( stored == false ) {
			$.ajax({
			    url: proxyUrl,
			    dataType: 'jsonp',
			    cache: false,
			    success: function (data) {
			    	if ( data.routes.length > 0 ) {
			    		var routes = {
							origin: typeof initial != "undefined" && initial.initial == true ? angular.element('#default-place').val() : angular.element('#origin').val(),
							destination: typeof initial != "undefined" && initial.initial == true ? angular.element('#default-place').val() : angular.element('#destination').val(),
							via: angular.element('#travelmode').val(),
							routes: data.routes[0]
						};

						localStorage.setItem( 'routes-' + routes.origin.split(' ').join('-') + '-' + routes.destination.split(' ').join('-') + '-' + angular.element('#travelmode').val(), JSON.stringify( routes ) );
						initializeMap(data.routes[0].legs[0], initial);
						console.log('make request');
						console.log('stored to local storage');
			    	}
			    	else {
			    		bootbox.hideAll();
			    		bootbox.alert('Status is: ' + data.status + '. Please modify your search query.', function() {});
			    	}
			    }
			});
		}
	}

	function storedData(initial) {
		// check first if localStorage is available
		if ( typeof Storage != "undefined" ) {
			console.log('web storage is available on your browser');
			var route_origin = typeof initial != "undefined" && initial.initial == true ? angular.element('#default-place').val().split(' ').join('-') : angular.element('#origin').val().split(' ').join('-');
			var route_destination = typeof initial != "undefined" && initial.initial == true ? angular.element('#default-place').val().split(' ').join('-') : angular.element('#destination').val().split(' ').join('-');

			if ( localStorage.getItem( 'routes-' + route_origin + '-' + route_destination + '-' + angular.element('#travelmode').val() ) != null ) {
				var ls_routes = JSON.parse(localStorage.getItem( 'routes-' + route_origin + '-' + route_destination + '-' + angular.element('#travelmode').val() ));
				if ( typeof ls_routes.origin != 'undefined' && ls_routes.origin == angular.element('#origin').val() && typeof ls_routes.destination != 'undefined' && ls_routes.destination == angular.element('#destination').val() ) {
					initializeMap(ls_routes.routes.legs[0]);
					$scope.mapapi.legs = ls_routes.routes.legs[0];
					console.log(ls_routes.routes);
					console.log('fetched from local storage');
					return true;
				}
				else {
					console.log('no local data stored');
					return false;
				}
			}
			else {
				console.log('no local data stored');
				return false;
			}
		}
		// web storage is not available, we render the data to our local object
		else {
			console.log('web storage is not available on your browser');
			return false;
		}
	}

	$scope.findRoute = function(initial) {
		var encodedRequest, proxyUrl;

		if ( typeof initial != "undefined" && initial.initial == true ) {
			$scope.mapapi.origin = 'origin=' + $scope.ui.default_place;
			$scope.mapapi.destination = '&destination=' + $scope.ui.default_place;
			$scope.mapapi.request = $scope.mapapi.url + $scope.mapapi.origin + $scope.mapapi.destination + '&language=' + $scope.ui.default_language + $scope.mapapi.sensor;
			encodedRequest = encodeURIComponent($scope.mapapi.request);
			proxyUrl   = 'http://jsonp.guffa.com/Proxy.ashx?url=' + encodedRequest;
			makeRequest(proxyUrl, initial);
		}
		else {
			$scope.mapapi.message = angular.element('#messages1').html();
			bootbox.alert($scope.mapapi.message, function() {});

			$scope.mapapi.origin = 'origin=' + $.trim(angular.element('#origin').val());
			$scope.mapapi.destination = '&destination=' + $.trim(angular.element('#destination').val());

			$scope.mapapi.origin_data = angular.element('#origin').val();
			$scope.mapapi.destination_data = angular.element('#destination').val();
			$scope.mapapi.travelmode_data = angular.element('#travelmode').val();

			if (angular.element('#travelmode').val() == 'TRANSIT') {
				var timestamp = new Date();
				timestamp = timestamp.getTime(angular.element('#departure').val());
				$scope.mapapi.departure_time = '&departure_time=' + Math.round( new Date().getTime()/1000 );
				$scope.mapapi.request = $scope.mapapi.url + $scope.mapapi.origin + $scope.mapapi.destination + '&language=' + $scope.ui.default_language + $scope.mapapi.sensor + $scope.mapapi.departure_time + $scope.mapapi.mode;
			}
			else {
				$scope.mapapi.request = $scope.mapapi.url + $scope.mapapi.origin + $scope.mapapi.destination + '&language=' + $scope.ui.default_language + $scope.mapapi.sensor;	
			}
			encodedRequest = encodeURIComponent($scope.mapapi.request);
			proxyUrl   = 'http://jsonp.guffa.com/Proxy.ashx?url=' + encodedRequest;

			makeRequest(proxyUrl);
		}

		jQuery('#controls').hide();
		jQuery('#routes-results').removeAttr('style');
		jQuery('#routes-results').show();
	}

	$scope.clearRoute = function() {
		blankFields();
	}

	$scope.attachLegendSlide = function() {
	    jQuery('#routes-results fieldset .content').each(function() {
	    	jQuery(this).hide();
	    });
	    jQuery('#routes-results legend').each(function() {
	    	jQuery(this).click(function(){
		        jQuery(this).parent().find('.content').slideToggle("fast");
		    });
	    });
	}

	$scope.renderSuggestion = function(route, index) {
		var directionsDisplay = new google.maps.DirectionsRenderer();
		calcRoute($scope.ui.origin, $scope.ui.destination, index, false);
		jQuery('fieldset').removeClass('active');
		jQuery('#fieldset'+index).addClass('active');
	}

	$scope.searchNew = function() {
		jQuery('#routes-results').animate({
			right:'-999'
		}, 400);
		jQuery('#controls').show();
	}

	$scope.loadPreviousSearches = function() {
		if (typeof Storage != 'undefined') {
			for (var ls in localStorage) {
				try {
					var n = JSON.parse(localStorage[ls]);
					if ( typeof n.origin != 'undefined' ) {
						$scope.ui.previous_searches.push( n );
					}
				}
				catch (err) {
					console.log(err);
				}
			}
		}
	}

	$scope.loadSettings = function() {
		$scope.ui.loadLanguage();
		$scope.ui.loadDefaultPlace();
		$scope.loadPreviousSearches();	
	}

	$scope.saveSettings = function() {
		$scope.ui.setMapLanguage();
		$scope.ui.setDefaultPlace();
		bootbox.dialog({
			message: angular.element('#messages2').html(),
			title: 'Route Planner Settings'
		});
		setTimeout('bootbox.hideAll()', 1000);
		setTimeout('window.location = window.location.href', 2000);
	}

	// initializeMap();
	$scope.loadSettings();
	$scope.findRoute({initial: true});

	jQuery(document).ready(function() {
		jQuery('#up').click(function() {
			var origin = angular.element('#origin').val();
			var destination = angular.element('#destination').val();
			jQuery('#origin').val( destination );
			jQuery('#destination').val( origin );
		});

		jQuery('#down').click(function() {
			var origin = angular.element('#origin').val();
			var destination = angular.element('#destination').val();
			jQuery('#origin').val( destination );
			jQuery('#destination').val( origin );
		});

		var pad = function(value) {
			return value < 10 ? "0" + value : value;
		}

		var date = new Date();
		var date1 = date.getFullYear() + '-' + pad(date.getMonth()+1) + '-' + pad(date.getDate()) + 'T' + pad(date.getHours()) + ':' + pad(date.getMinutes());

		jQuery('#departure').val(date1);


	    jQuery('fieldset .content').each(function() {
	    	jQuery(this).hide();
	    });

	    jQuery('legend').each(function() {
    		jQuery(this).click(function(){
		        jQuery(this).parent().find('.content').slideToggle("fast");

		        $scope.$apply(function() {
		        	jQuery('.ps-items .ps-item').each(function() {
			        	jQuery(this).click(function() {
			        		angular.element('#origin').val( jQuery(this).find('.origin-data').html() );
			        		angular.element('#destination').val( jQuery(this).find('.destination-data').html() );
			        		angular.element('#travelmode').val( jQuery(this).find('.via-data').html() );
			        		jQuery('#travelmode').trigger('change');
			        	});
			        });
		        });
		    });
	    });

		

	    jQuery('#travelmode').change(function() {
	    	if ( jQuery(this).val() == 'TRANSIT' ) {
	    		jQuery('#transit-vehicles').removeClass('hidden');
	    	}
	    	else {
	    		jQuery('#transit-vehicles').addClass('hidden');	
	    	}
	    });

	    jQuery('#travelmode').trigger('change');

	    jQuery('.selectpicker').selectpicker({
	    	style:'btn-success'
	    });
		 
	});
}
